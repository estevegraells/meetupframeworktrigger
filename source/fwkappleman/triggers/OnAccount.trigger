/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description: Trigger para Account que invoca al Dispatcher
 */

trigger OnAccount on Account (
        before insert, before update,
        before delete, after insert,
        after update, after delete, after undelete) {

    // Llamada al dispatcher del framework de Dan Appleman
    GlobalDispatcher.run('Account',
            Trigger.isBefore, Trigger.isAfter,
            Trigger.isInsert, Trigger.isUpdate,
            Trigger.isDelete, Trigger.isExecuting,
            Trigger.new, Trigger.newMap,
            Trigger.old, Trigger.oldMap);

   // Elimina la dependencia que introducía Scott
   // Hay una carga de parámetros en las funciones
}