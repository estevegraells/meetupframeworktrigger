/*
 * @author: Dan Appleman con ajustes de Esteve Graells 
 * @date: junio 2018
 * @description: Implantación de la lógica de negocio para la gestión de los
 * triggers de Account
 *
 */


public class AccountEntryHandler implements iTriggerEntry {

    static Set<Id> accIdsWithOpps = new Set<Id>();

    public void mainEntry(
            String triggerObject,
            Boolean isBefore,
            Boolean isAfter,
            Boolean isInsert,
            Boolean isUpdate,
            Boolean isDelete,
            Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        List<Account> accNewList = (List<Account>) newList;
        List<Account> accOldList = (List<Account>) oldList;
        Map<ID, Account> accNewMap = (Map<ID, Account>) newMap;
        Map<ID, Account> accOldMap = (Map<ID, Account>) oldMap;

        if (triggerObject == 'Account') {

            if (isBefore){

                if (isInsert){
                    //// Ejecución de Bloque funcional correspondiente
                    System.debug('-ege- BeforeInsert ' + triggerObject);}
                if (isUpdate){
                    //FunctionalBlock2()
                    System.debug('-ege- BeforeUpdate ' + triggerObject);}
                if (isDelete) {
                    //// Ejecución de Bloque funcional correspondiente
                    System.debug('-ege- BeforeDelete ' + triggerObject);
                    beforeDeleteFunctionalBlock(isBefore, isAfter, accNewList, accNewMap, accOldList, accOldMap);
                }

            }else if (isAfter){
                if (isInsert){
                    //// Ejecución de Bloque funcional correspondiente
                    System.debug('-ege- AfterInsert ' + triggerObject);}
                if (isUpdate){
                    //// Ejecución de Bloque funcional correspondiente
                    System.debug('-ege- AfterUpdate ' + triggerObject);}
                if (isDelete) {
                    //// Ejecución de Bloque funcional correspondiente
                    System.debug('-ege- AfterDelete ' + triggerObject);
                    afterDeleteFunctionalBlock(isBefore, isAfter, accNewList, accNewMap, accOldList, accOldMap);
                }
            }
        }
    }

    //Gestión de la re-entrada para Account
    public void inProgressEntry(
            String triggerObject,
            Boolean isBefore,
            Boolean isAfter,
            Boolean isInsert,
            Boolean isUpdate,
            Boolean isDelete,
            Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        // Be sure to detect for the objects you actually want to handle.

        if (TriggerObject == 'Account' && IsAfter) {
            // Do some processing here
            // Can dispatch to other classes is necessary
        }
    }

    // No permitir el borrado de Accounts con Oportunidades
    // Es un ejemplo de Bloque funcional

    private void beforeDeleteFunctionalBlock(
            Boolean isBefore,
            Boolean isAfter,
            List<Account> newList, Map<ID, Account> newMap,
            List<Account> oldList, Map<ID, Account> oldMap){

        // Dependiendo de la Naturaleza del trigger los valores
        // de las Accounts vienen en un Map u otro

        Set<Id> accIds = oldMap.keySet();

        //Obtener las Accounts con sus Oportunidades
        for (Account[] accounts : [
                SELECT a.Id, (SELECT Id FROM Opportunities LIMIT 1)
                FROM Account a
                WHERE a.Id in :accIds]) {

            for (Account acc : accounts) {

                if (acc.Opportunities.size() > 0) {
                    accIdsWithOpps.add(acc.Id);
                    acc.addError('Esta account no se puede eliminar, tiene oportunidades asociadas');
                }
            }
        }
    }


    // No permitir el borrado de Accounts con Oportunidades
    // Es otro ejemplo de Bloque funcional
    private void afterDeleteFunctionalBlock(
            Boolean isBefore,
            Boolean isAfter,
            List<Account> newList, Map<ID, Account> newMap,
            List<Account> oldList, Map<ID, Account> oldMap){

        // No permitir el borrado de los Accounts que tienen Opportunities

        for ( Id accId : oldMap.keySet() ){

            if (accIdsWithOpps.contains(accId)){
                oldMap.get(accId).addError('Esta account no se puede eliminar, tiene oportunidades asociadas');
            }
        }
    }
}