/*
 * @author: Dan Appleman con ajustes de Esteve Graells 
 * @date: junio 2018
 * @description: Implantación de la lógica de negocio para
 * la gestión de los triggers de Opportuntiy
 *
 */

public class OpportunityEntryHandler implements iTriggerEntry{

    //Función de gestión del trigger en primera entrada
    public void mainEntry(
            String triggerObject,
            Boolean isBefore,
            Boolean isAfter,
            Boolean isInsert,
            Boolean isUpdate,
            Boolean isDelete,
            Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        List<Opportunity> opNewList = (List<Opportunity>) newList;
        List<Opportunity> opOldList = (List<Opportunity>) oldList;
        Map<ID, Opportunity> opNewMap = (Map<ID, Opportunity>) newMap;
        Map<ID, Opportunity> opOldMap = (Map<ID, Opportunity>) oldMap;

        //Implementación de la logica de negocio que se desea
        // para cada evento
        if (triggerObject == 'Opportunity'){

            if (isBefore){

                if (isInsert){
                    //FunctionalBlock1()
                    System.debug('-ege- BeforeInsert ' + triggerObject);}
                if (isUpdate){
                    //FunctionalBlock2()
                    System.debug('-ege- BeforeUpdate ' + triggerObject);}
                if (isDelete){
                    //FunctionalBlockn()
                    System.debug('-ege- BeforeDelete ' + triggerObject);}
            }else if (isAfter){
                if (isInsert){
                    //FunctionalBlockn()
                    System.debug('-ege- AfterInsert ' + triggerObject);}
                if (isUpdate){
                    //FunctionalBlockn()
                    System.debug('-ege- AfterUpdate ' + triggerObject);}
                if (isDelete){
                    //FunctionalBlockn()
                    System.debug('-ege- AfterDelete ' + triggerObject);}
            }
        }
    }

    //Método que gestiona las re-entradas de Oportunidades
    public void inProgressEntry(
            String triggerObject,
            Boolean isBefore,
            Boolean isAfter,
            Boolean isInsert,
            Boolean isUpdate,
            Boolean isDelete,
            Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        if (TriggerObject == 'Opportunity') {

            //Logica de negocio
            System.debug('-ege- Re-entrada del trigger para ' + triggerObject);
            //FunctionalBlockn()

        }
    }
}