/*
* @author Hari Krishnan 
* @date 07/17/2013 y 06/2018
*/

public class AccountAfterUpdateTriggerHandler extends TriggerHandlerBase {

	//Lógica de negocio para trigger sin re-entrada
	public override void mainEntry(TriggerParameters tp) {
		process((List<Account>)tp.newList);

		//Sería adecuado invocar métodos via la clase AccountHelper
		//para que otros Handlers de Account pudieran utilizarlo 
	}
	
	private void process(List<Account> listNewAccounts) {
		for(Account acct : listNewAccounts) {
			Account newAccount = new Account();
			newAccount.Id = acct.Id;
			newAccount.Website = 'www.salesforce.com';
			sObjectsToUpdate.put(newAccount.Id, newAccount);
		}
	}
	
	//Lógica de negocio para trigger sin re-entrada
	public override void inProgressEntry(TriggerParameters tp) {
		System.debug('This is an example for reentrant code...');
	}
	
	public override void updateObjects() {
		// for demonstration purposes, don't do anything here...
	}
}