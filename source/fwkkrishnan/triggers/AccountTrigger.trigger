/*
* @author: Dan Appleman con ajustes de Esteve Graells 
* @date: Junio 2018
*
*/
trigger AccountTrigger on Account (after insert, after update, before insert, before update) {
	// This is the only line of code that is required.

	//Esteve: en mi opinión esta es la invocación más orientada a OOP y la más adecuada
	//no utiliza ni el class del Handler como Scott ni un String como Appleman
	TriggerFactory.createTriggerDispatcher(Account.sObjectType);
}