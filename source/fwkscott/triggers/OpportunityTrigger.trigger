/* @author: Tony Scott con ajustes de Esteve Graells 
 * @date: junio 2018
 * @description: Trigger para el objeto Opportunity
 */

trigger OppTrigger on Opportunity (
        before insert, before update,
        before delete, after insert,
        after update, after delete, after undelete) {

    TriggerFactory.createAndExecuteHandler(OpportunityHandler2.class);

}