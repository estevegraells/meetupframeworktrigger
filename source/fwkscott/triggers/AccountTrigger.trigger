/* @author: Tony Scott con ajustes de Esteve Graells 
 * @date: junio 2018
 * @description: Trigger para el objeto Account
 */

trigger AccountTrigger on Account (
        before insert, before update,
        before delete, after insert,
        after update, after delete, after undelete) {

    //Invocamos a la Factory indicando el Handler que queremos utilizar
    TriggerFactory.createAndExecuteHandler(AccountHandler2.class);
    
    //Establecer una dependencia adicional innecesaria entre la Factory y el Handler
    //cuando realmente la dependencia debería ser con el objeto

}