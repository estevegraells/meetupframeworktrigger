/*
 * @author: Tony Scott con ajustes de Esteve Graells 
 * @date: junio 2018
 * @description: crea la instancia del Handler del objeto que estemos gestionando (y de ahí su nombre)
 * y enruta hacia la función adecuada del evento
 *
 */
public with sharing class TriggerFactory{

    /* 
    * Función principal donde se realiza la creación dinámica del Handler y el enrutamiento
    * @param t Tipo de la clase Handler que será instanciado y que debe implementar la interface ITrigger
    */
    public static void createAndExecuteHandler(Type t){

        ITrigger handler = getHandler(t);

        if (Handler == null) throw new TriggerException('No Trigger Handler: ' + t.getName());

        // Ejecutar el enrutado hacia la función que
        // implementa el evento generado

        execute(handler);
    }

    /*
    * Función para la creación dinámica del handler
    * @param t Tipo de la clase Handler que será instanciado y que debe implementar la interface ITrigger
    * @return La instancia creada  del handler o null
    */
    private static ITrigger getHandler(Type t){
        Object o = t.newInstance();

        // Comprobación de seguridad
        if (o instanceOf ITrigger)return (ITrigger)o;
        else return null;
    }
    

    /*
    * Ejecución del enrutado
    * @param Handler de la clase del objeto
    */
    private static void execute(ITrigger handler){

        if (Trigger.isBefore){

            // Función de caché
            handler.bulkBefore();

            if (Trigger.isDelete){
                for (SObject so : Trigger.old){
                    handler.beforeDelete(so);
                }
            }

            else if (Trigger.isInsert){
                for (SObject so : Trigger.new){
                    handler.beforeInsert(so);
                }
            }

            else if (Trigger.isUpdate){
                for (SObject so : Trigger.old){
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }

        //Repetimos el esquema hacia las funciones after

        else{

            handler.bulkAfter();

            if (Trigger.isDelete){
                for (SObject so : Trigger.old){
                    handler.afterDelete(so);
                }
            }

            else if (Trigger.isInsert){
                for (SObject so : Trigger.new){
                    handler.afterInsert(so);
                }
            }

            else if (Trigger.isUpdate){
                for (SObject so : Trigger.old){
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }

        // Post-proceso y operaciones DML
        handler.andFinally();
    }

    

    /*
    * El código de la Excepcion, puede ser el que deseemos
    */
    public class TriggerException extends Exception {}
}